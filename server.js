 try {
     var connect = require('connect');
 } catch (e) {
     console.log('To run the server, you must ' +
         'install the connect module:\n\n' +
         'npm install connect');
     return;
 }

 var port = 8080;
 connect.createServer(
     connect.static(__dirname)).listen(port);
 console.log("start server on port : " + port);
